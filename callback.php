<?php
	require_once("config.php");

	try {
		$accessToken = $handler->getAccessToken();
	}catch(\Facebook\Exceptions\FacebookResponseException $e){
		echo "Response Exception: " . $e->getMessage();
		exit();
	}


	if(!$accessToken){
		header('Location: login.php');
		exit();
	}

	$oAuth2Client = $fbObject->getOAuth2Client();
	if(!$accessToken->isLongLived()){
		$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
	}

	$myResponse = $fbObject->get("/me?fields=id, name, first_name, middle_name, last_name, name_format, short_name, location, hometown, languages, link, is_guest_user, is_shared_login, age_range, security_settings, sports, video_upload_limits, viewer_can_send_gift, gender, quotes, friends, payment_pricepoints, albums, books, feed, photos, taggable_friends", $accessToken);

	$postsResponse = $fbObject->get("/me/posts?fields=comments, attachments, picture, full_picture, dynamic_posts, reactions, sharedposts, sponsor_tags, to", $accessToken);

	$userData = $myResponse->getGraphNode()->asArray();
	$postData = $postsResponse->getGraphEdge()->asArray();

	$_SESSION['userData'] = $userData;
	$_SESSION['postData'] = $postData;
	$_SESSION['accessToken'] = (string) $accessToken;

	header('Location: index.php');
	
?>