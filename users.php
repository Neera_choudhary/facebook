<?php
	require_once('config.php');
	// echo "<pre>"; print_r($_SESSION['userData']);
?>
<!DOCTYPE html>
<html>
<head>
	<title>USER</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
</head>
<body>
	<h3>User's name details</h3>
	<table class="table-bordered">
		<thead>
			<tr>
				<th>id</th>
				<th>full name</th>
				<th>first name</th>
				<th>last name</th>
				<th>short name</th>
				<th>name format</th>				
			</tr>
		</thead>
		<tbody>
			<tr>
				<td> <?php echo $_SESSION['userData']['id']; ?> </td>
				<td> <?php echo $_SESSION['userData']['name']; ?> </td>
				<td> <?php echo $_SESSION['userData']['first_name']; ?> </td>
				<td> <?php echo $_SESSION['userData']['last_name']; ?> </td>
				<td> <?php echo $_SESSION['userData']['short_name']; ?> </td>
				<td> <?php echo $_SESSION['userData']['name_format']; ?> </td>
			</tr>
		</tbody>
	</table>

	<h3>User's address</h3>
	<table class="table-bordered">
		<thead>
			<tr>
				<th>Current location</th>
				<th>Hometown</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td> <?php echo"<pre>";print_r($_SESSION['userData']['location']) ?> </td>
				<td> <?php echo"<pre>";print_r($_SESSION['userData']['hometown']); ?> </td>

			</tr>
		</tbody>
	</table>

	<h3>User's profile link</h3>
	<a href=" <?php echo $_SESSION['userData']['link'] ?> "> Click to visit profile </a>

	<h3>User's additional information</h3>
	<table class="table-bordered">
		<thead>
			<th>Age range</th>
			<th>Friends count</th>
			<th>Guest user</th>
			<th>Shared login</th>
			<th>Sequrity settings</th>
			<th>Video upload limit</th>
			<th>Viewer can send gift</th>
		</thead>
		<tbody>
			<td> <?php echo"<pre>";print_r($_SESSION['userData']['age_range']); ?> </td>
			<td>  </td>
			<td> <?php echo (!empty($_SESSION['userData']['is_guest_user'])) ? $_SESSION['userData']['is_guest_user'] : "N/A"?> </td>
			<td> <?php echo (!empty($_SESSION['userData']['is_shared_login'])) ? $_SESSION['userData']['is_shared_login'] : "N/A" ?> </td>
			<td> <?php echo"<pre>";print_r($_SESSION['userData']['security_settings']); ?> </td>
			<td> <?php echo"<pre>";print_r($_SESSION['userData']['video_upload_limits']); ?> </td>
			<td> <?php echo (!empty($_SESSION['userData']['viewer_can_send_gift'])) ? $_SESSION['userData']['viewer_can_send_gift'] : "N/A" ?> </td>
		</tbody>
	</table>

	<h3>User's payment pricepoint</h3>
	<table>
		<thead>
			<?php echo"<pre>";print_r($_SESSION['userData']['payment_pricepoints']); ?>
		</thead>
	</table>

	<h3>User's albums</h3>
	<table>
		<thead>
			<?php echo"<pre>";print_r($_SESSION['userData']['albums']); ?>
		</thead>
	</table>

	<h3>User's liked Books</h3>
	<table>
		<thead>
			<?php echo"<pre>";print_r($_SESSION['userData']['books']); ?>
		</thead>
	</table>

	<h3>User's news feeds</h3>
	<table>
		<thead>
			<?php echo"<pre>";print_r($_SESSION['userData']['feed']); ?>
		</thead>
	</table>

	<h3>User's Photos details</h3>
	<table>
		<thead>
			<?php echo"<pre>";print_r($_SESSION['userData']['photos']); ?>
		</thead>
	</table>
</body>
</html>