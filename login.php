<?php
    require_once('config.php');

    $redirectTo = "http://localhost/facebook/callback.php";
    
    $permissions = [
                    'user_location',
                    'user_hometown',
                    'user_link',
                    'user_age_range',
                    'user_posts',
                    'user_gender',
                    'user_likes',
                    'user_friends',
                    'user_photos',
                    'user_posts'
                  ];

    $fullUrl = $handler->getLoginUrl($redirectTo, $permissions);
?>

<!DOCTYPE html>
<html>
<head>
	<title>Login page</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
</head>
<body>
    <form class="form-group col-lg-8" style="margin-top:2rem;">
        <div class="form-group col-lg-3">
            Username
            <input type="text" class="form-control" name="email" placeholder="Enter username">
        </div>

        <div class="form-group col-lg-3">
            Password
            <input type="password" class="form-control" name="password" placeholder="Enter password">
        </div>

        <div class="form-group col-md-3 text-center">
            <input type="submit" name="submit" class="btn btn-primary"></button>
        </div>

        <div class="form-group col-md-3 text-center">
            <input type="button" onclick="window.location = '<?php echo $fullUrl; ?>'" name="fb" value="Login through Facebook" class="btn btn-primary"></button>
        </div>
    </form>
</body>
</html>