<?php
	require_once("config.php");
// print_r($_SESSION); die;
	if(!isset($_SESSION['accessToken'])){
		header('Location: login.php');
		exit();
	}else{
?>
<!DOCTYPE html>
<html>
<head>
	<title>Selection page</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
</head>
<body>
	<form method="POST" action="users.php" style="margin-top:2rem; margin-left: 2rem;">
		<button type="submit" name="user" class="btn btn-success">User Information</button>
	</form>
	<form method="POST" action="posts.php" style="margin-top:2rem; margin-left: 2rem;">
		<button type="submit" name="user" class="btn btn-danger">Post Information</button>
	</form>
</body>
<div class="printData">
	
</div>
</html>
<?php
	}
?>

